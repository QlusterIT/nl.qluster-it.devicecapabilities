let categories =  [            
  {
      title:'Devices',
      icons:[
          {name:"device_1", file:'custom_1', defaultIcon:'Devices/device_capabilities', title:{en:"Device 1", nl:"Apparaat 1", de:""}},
          {name:"device_2", file:'custom_2', defaultIcon:'Devices/virtual_device', title:{en:"Device 2", nl:"Apparaat 2", de:""}},
          {name:"device_3", file:'custom_3', defaultIcon:'Devices/circle', title:{en:"Device 3", nl:"Apparaat 3", de:""}},
          {name:"device_4", file:'custom_4', defaultIcon:'Devices/curtain-half', title:{en:"Device 4", nl:"Apparaat 4", de:""}},
          {name:"device_5", file:'custom_5', defaultIcon:'Devices/game', title:{en:"Device 5", nl:"Apparaat 5", de:""}},
          {name:"device_6", file:'custom_6', defaultIcon:'Devices/onoff', title:{en:"Device 6", nl:"Apparaat 6", de:""}},
          {name:"device_7", file:'custom_7', defaultIcon:'Devices/settings', title:{en:"Device 7", nl:"Apparaat 7", de:""}},
          {name:"device_8", file:'custom_8', defaultIcon:'Devices/tv', title:{en:"Device 8", nl:"Apparaat 8", de:""}},
          {name:"device_9", file:'custom_9', defaultIcon:'Buttons/button', title:{en:"Device 9", nl:"Apparaat 9", de:""}},
          {name:"device_10", file:'custom_10', defaultIcon:'Buttons/devices', title:{en:"Device 10", nl:"Apparaat 10", de:""}},
          {name:"device_11", file:'custom_11', defaultIcon:'Buttons/energy', title:{en:"Device 11", nl:"Apparaat 11", de:""}},
          {name:"device_12", file:'custom_12', defaultIcon:'Buttons/insights', title:{en:"Device 12", nl:"Apparaat 12", de:""}},
          {name:"device_13", file:'custom_13', defaultIcon:'Buttons/home', title:{en:"Device 13", nl:"Apparaat 13", de:""}},
          {name:"device_14", file:'custom_14', defaultIcon:'Buttons/homeyscript', title:{en:"Device 14", nl:"Apparaat 14", de:""}},
          {name:"device_15", file:'custom_15', defaultIcon:'Buttons/search', title:{en:"Device 15", nl:"Apparaat 15", de:""}},
          {name:"device_16", file:'custom_16', defaultIcon:'Buttons/ternary', title:{en:"Device 16", nl:"Apparaat 16", de:""}},
          {name:"device_17", file:'custom_17', defaultIcon:'Buttons/toggle', title:{en:"Device 17", nl:"Apparaat 17", de:""}},
          {name:"device_18", file:'custom_18', defaultIcon:'Flows/advanced-flow', title:{en:"Device 18", nl:"Apparaat 18", de:""}},
          {name:"device_19", file:'custom_19', defaultIcon:'Flows/flow-bold', title:{en:"Device 19", nl:"Apparaat 19", de:""}},
          {name:"device_20", file:'custom_20', defaultIcon:'Flows/logic', title:{en:"Device 20", nl:"Apparaat 20", de:""}}
      ]
  },
  {
      title:'Capabilities', 
      icons:[
          {name:"capabilities_1", file:'custom_21', defaultIcon:'Other/measure_clouds', title:{en:"Fields 1", nl:"Velden 1", de:""}},
          {name:"capabilities_2", file:'custom_22', defaultIcon:'Other/measure_humidity', title:{en:"Fields 2", nl:"Velden 2", de:""}},
          {name:"capabilities_3", file:'custom_23', defaultIcon:'Other/measure_luminance', title:{en:"Fields 3", nl:"Velden 3", de:""}},
          {name:"capabilities_4", file:'custom_24', defaultIcon:'Other/measure_power', title:{en:"Fields 4", nl:"Velden 4", de:""}},
          {name:"capabilities_5", file:'custom_25', defaultIcon:'Other/measure_rain', title:{en:"Fields 5", nl:"Velden 5", de:""}},
          {name:"capabilities_6", file:'custom_26', defaultIcon:'Other/measure_temperature', title:{en:"Fields 6", nl:"Velden 6", de:""}},
          {name:"capabilities_7", file:'custom_27', defaultIcon:'Other/measure_wind_angle', title:{en:"Fields 7", nl:"Velden 7", de:""}},
          {name:"capabilities_8", file:'custom_28', defaultIcon:'Other/measure_wind_strength', title:{en:"Fields 8", nl:"Velden 8", de:""}},
          {name:"capabilities_9", file:'custom_29', defaultIcon:'Other/heart-outline', title:{en:"Fields 9", nl:"Velden 9", de:""}},
          {name:"capabilities_10", file:'custom_30', defaultIcon:'Other/bell', title:{en:"Fields 10", nl:"Velden 10", de:""}},
          {name:"capabilities_11", file:'custom_31', defaultIcon:'Other/moon-round', title:{en:"Fields 11", nl:"Velden 11", de:""}},
          {name:"capabilities_12", file:'custom_32', defaultIcon:'Gauges_and_meters/measure_noise', title:{en:"Fields 12", nl:"Velden 12", de:""}},
          {name:"capabilities_13", file:'custom_33', defaultIcon:'Gauges_and_meters/measure_pressure', title:{en:"Fields 13", nl:"Velden 13", de:""}},
          {name:"capabilities_14", file:'custom_34', defaultIcon:'Gauges_and_meters/meter_gas', title:{en:"Fields 14", nl:"Velden 14", de:""}},
          {name:"capabilities_15", file:'custom_35', defaultIcon:'Gauges_and_meters/meter_power', title:{en:"Fields 15", nl:"Velden 15", de:""}},
          {name:"capabilities_16", file:'custom_36', defaultIcon:'Gauges_and_meters/sensor', title:{en:"Fields 16", nl:"Velden 16", de:""}},
          {name:"capabilities_17", file:'custom_37', defaultIcon:'Alarms/alarm_smoke', title:{en:"Fields 17", nl:"Velden 17", de:""}},
          {name:"capabilities_18", file:'custom_38', defaultIcon:'Alarms/alarm_water', title:{en:"Fields 18", nl:"Velden 18", de:""}},
          {name:"capabilities_19", file:'custom_39', defaultIcon:'Devices/battery', title:{en:"Fields 19", nl:"Velden 19", de:""}},
          {name:"capabilities_20", file:'custom_40', defaultIcon:'Flows/tag', title:{en:"Fields 20", nl:"Velden 20", de:""}},

          {name:"capabilities_21", file:'custom_61', defaultIcon:'Kawa/clock', title:{en:"Fields 21", nl:"Velden 21", de:""}},
          {name:"capabilities_22", file:'custom_62', defaultIcon:'Kawa/cooking_icon', title:{en:"Fields 22", nl:"Velden 22", de:""}},
          {name:"capabilities_23", file:'custom_63', defaultIcon:'Kawa/doorbell2', title:{en:"Fields 23", nl:"Velden 23", de:""}},
          {name:"capabilities_24", file:'custom_64', defaultIcon:'Kawa/fan_big_black', title:{en:"Fields 24", nl:"Velden 24", de:""}},
          {name:"capabilities_25", file:'custom_65', defaultIcon:'Kawa/fan_icon', title:{en:"Fields 25", nl:"Velden 25", de:""}},
          {name:"capabilities_26", file:'custom_66', defaultIcon:'Kawa/fireplace_icon', title:{en:"Fields 26", nl:"Velden 26", de:""}},
          {name:"capabilities_27", file:'custom_67', defaultIcon:'Kawa/ggl_hub', title:{en:"Fields 27", nl:"Velden 27", de:""}},
          {name:"capabilities_28", file:'custom_68', defaultIcon:'Kawa/homey_fake', title:{en:"Fields 28", nl:"Velden 28", de:""}},
          {name:"capabilities_29", file:'custom_69', defaultIcon:'Kawa/homey_pro', title:{en:"Fields 29", nl:"Velden 29", de:""}},
          {name:"capabilities_30", file:'custom_70', defaultIcon:'Kawa/kitchen_icon', title:{en:"Fields 30", nl:"Velden 30", de:""}},
          {name:"capabilities_31", file:'custom_71', defaultIcon:'Kawa/lamp_desklight_icon', title:{en:"Fields 31", nl:"Velden 31", de:""}},
          {name:"capabilities_32", file:'custom_72', defaultIcon:'Kawa/light_ceilinglight', title:{en:"Fields 32", nl:"Velden 32", de:""}},
          {name:"capabilities_33", file:'custom_73', defaultIcon:'Kawa/location-pin-compact-outline', title:{en:"Fields 33", nl:"Velden 33", de:""}},
          {name:"capabilities_34", file:'custom_74', defaultIcon:'Kawa/lock_fingerprint2', title:{en:"Fields 34", nl:"Velden 34", de:""}},
          {name:"capabilities_35", file:'custom_75', defaultIcon:'Kawa/microwave', title:{en:"Fields 35", nl:"Velden 35", de:""}},
          {name:"capabilities_36", file:'custom_76', defaultIcon:'Kawa/relay', title:{en:"Fields 36", nl:"Velden 36", de:""}},
          {name:"capabilities_37", file:'custom_77', defaultIcon:'Kawa/remote_bluebtn', title:{en:"Fields 37", nl:"Velden 37", de:""}},
          {name:"capabilities_38", file:'custom_78', defaultIcon:'Kawa/solarpower', title:{en:"Fields 38", nl:"Velden 38", de:""}},
          {name:"capabilities_39", file:'custom_79', defaultIcon:'Kawa/stove_icon', title:{en:"Fields 39", nl:"Velden 39", de:""}},
          {name:"capabilities_40", file:'custom_80', defaultIcon:'Kawa/wc_icon', title:{en:"Fields 40", nl:"Velden 40", de:""}}
      ]
  },
  {
      title:'Other',
      icons:[
          {name:"other_1", file:'custom_41', defaultIcon:'Media/music', title:{en:"Other 1", nl:"Overige 1", de:""}},
          {name:"other_2", file:'custom_42', defaultIcon:'Media/speaker_next', title:{en:"Other 2", nl:"Overige 2", de:""}},
          {name:"other_3", file:'custom_43', defaultIcon:'Media/speaker_play', title:{en:"Other 3", nl:"Overige 3", de:""}},
          {name:"other_4", file:'custom_44', defaultIcon:'Media/speaker_prev', title:{en:"Other 4", nl:"Overige 4", de:""}},
          {name:"other_5", file:'custom_45', defaultIcon:'Media/volume_down', title:{en:"Other 5", nl:"Overige 5", de:""}},
          {name:"other_6", file:'custom_46', defaultIcon:'Media/volume_mute', title:{en:"Other 6", nl:"Overige 6", de:""}},
          {name:"other_7", file:'custom_47', defaultIcon:'Media/volume_up', title:{en:"Other 7", nl:"Overige 7", de:""}},
          {name:"other_8", file:'custom_48', defaultIcon:'Weather/bewolkt-blk', title:{en:"Other 8", nl:"Overige 8", de:""}},
          {name:"other_9", file:'custom_49', defaultIcon:'Weather/dampn-blk', title:{en:"Other 9", nl:"Overige 9", de:""}},
          {name:"other_10", file:'custom_50', defaultIcon:'Weather/hagel-blk', title:{en:"Other 10", nl:"Overige 10", de:""}},
          {name:"other_11", file:'custom_51', defaultIcon:'Weather/hardewind-blk', title:{en:"Other 11", nl:"Overige 11", de:""}},
          {name:"other_12", file:'custom_52', defaultIcon:'Weather/regen-blk', title:{en:"Other 12", nl:"Overige 12", de:""}},
          {name:"other_13", file:'custom_53', defaultIcon:'Weather/storm-blk', title:{en:"Other 13", nl:"Overige 13", de:""}},
          {name:"other_14", file:'custom_54', defaultIcon:'Kawa/watch', title:{en:"Other 14", nl:"Overige 14", de:""}},
          {name:"other_15", file:'custom_55', defaultIcon:'Weather/zeerkoud-blk', title:{en:"Other 15", nl:"Overige 15", de:""}},
          {name:"other_16", file:'custom_56', defaultIcon:'Weather/zeerwarm-blk', title:{en:"Other 16", nl:"Overige 16", de:""}},
          {name:"other_17", file:'custom_57', defaultIcon:'Weather/zonnigeperioden-blk', title:{en:"Other 17", nl:"Overige 17", de:""}},
          {name:"other_18", file:'custom_58', defaultIcon:'Weather/zonnign-blk', title:{en:"Other 18", nl:"Overige 18", de:""}},
          {name:"other_19", file:'custom_59', defaultIcon:'Kawa/bathroom_icon', title:{en:"Other 19", nl:"Overige 19", de:""}},
          {name:"other_20", file:'custom_60', defaultIcon:'Kawa/human', title:{en:"Other 20", nl:"Overige 20", de:""}}
      ]
  }
];


if(false) {
  let newIcons = [''];
  for(let i=0;i<categories.length;i++)
    for(let j=0;j<categories[i].icons.length;j++)
      newIcons.push(categories[i].icons[j].file);

  console.log(newIcons);
  return;
} 

let str='';
for(let i=0;i<categories.length;i++){
  str+= '\'                {"id": "'+"".padStart(i+1,' ')+'","label": {"en": "-----   '+categories[i].title.padEnd(15, ' ')+'   -----"}}, \\r\\n\' + \r\n';
    for(let j=0;j<categories[i].icons.length;j++) {
      var icon  = categories[i].icons[j];
      str+= '\'                {"id": "'+icon.file+'","label": ' + JSON.stringify(icon.title) + '}';
      if(i===categories.length-1 && j===categories[i].icons.length-1)str+="\\r\\n\' + \r\n";else str+=", \\r\\n\' + \r\n";
    }
}
console.log(str);

//'                {"id": "","label": {"en": ""}}, \r\n' +
